# frozen_string_literal: true

class UsersController < ApplicationController
  load_and_authorize_resource
  
  def index
    @users = User.all
    @users = @users.where(role: params[:role]) if params[:role]
  end

  def new
    @user = User.new
  end

  def create
    @user = User.new(user_params)
    @user.skip_password_validation = true

    respond_to do |format|
      if @user.save
        format.html { redirect_to users_url(role: @user.role), notice: "User was successfully created." }
        format.json { render json: @user.to_json, status: :created }
      else
        format.html { render :new, status: :unprocessable_entity }
        format.json { render json: @user.errors, status: :unprocessable_entity }
      end
    end
  end

  private

  def user_params
    params.require(:user).permit(:first_name, :email, :last_name, :role)
  end
end
