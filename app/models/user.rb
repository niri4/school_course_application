class User < ApplicationRecord
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable, :trackable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :validatable
  attr_accessor :skip_password_validation

  enum role: { admin: 0, school_admin: 1, student: 2 }

  validates :first_name, presence: true
  validates :last_name, presence: true
  validates :email, presence: true, uniqueness: { case_sensitive: true }, format: URI::MailTo::EMAIL_REGEXP

  has_many :enrollment_requests, foreign_key: "student_id", class_name: "EnrollmentRequest"
  has_many :batches, -> { where(enrollment_requests: { status: 'approved'}) }, through: :enrollment_requests


  def full_name
    "#{first_name} #{last_name}"
  end

  def classmates(batch_id:)
    return [] if batches.find_by(id: batch_id).nil?
    EnrollmentRequest.where(batch_id: batch_id).where.not(student_id: id)
  end

  protected

  def password_required?
    return false if skip_password_validation
    super
  end
end
