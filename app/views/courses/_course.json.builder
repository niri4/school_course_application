json.extract! course, :id, :created_at, :updated_at
json.url school_course_url(school_id: @school.id, id: @course.id, format: :json)