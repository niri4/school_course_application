# frozen_string_literal: true

require "rails_helper"

describe BatchesController do
  let(:student) { create(:user, role: "student") }
  let(:school_admin) { create(:user, role: "school_admin") }
  let(:admin) { create(:user, role: "admin") }
  let(:school) { create(:school, admin: school_admin) }
  let(:course) { create(:course, school: school) }

  context "GET #index" do
    it "should get all the batches" do
      create_list(:batch, 2, course: course, school: school)
      sign_in student

      get :index, params: { school_id: school.id, course_id: course.id }

      expect(assigns[:batches].size).to eq(2)
    end
  end

  context "GET #show" do
    it "should not show batch detail" do
      sign_in admin

      get :show, params: { school_id: school.id, course_id: course.id, id: "alpha" }

      expect(response.body).to include("404")
      expect(response.status).to eq(302)
    end

    it "should show batch detail" do
      batch = create(:batch, course: course, school: school)
      student2  =  create(:user, role: "student")
      create(:enrollment_request, batch_id: batch.id, student: student)
      create(:enrollment_request, batch_id: batch.id, student: student2, approved_by: school_admin.id, approved_at: Time.now)

      sign_in admin

      get :show, params: { school_id: school.id, course_id: course.id, id: batch.id }

      expect(assigns[:total_pending_enrollments]).to eq(batch.enrollment_requests.pending.count)
      expect(assigns[:total_approved_enrollments]).to eq(batch.enrollment_requests.approved.count)
    end
  end

  context "GET #pending_enrollment_requests" do
    it "should return pending enrollment request" do
      batch = create(:batch, course: course, school: school)
      student2 = create(:user, role: "student")
      create(:enrollment_request, batch_id: batch.id, student: student)
      create(:enrollment_request, batch_id: batch.id, student: student2, status: "approved", approved_by: school_admin.id, approved_at: Time.now)
      sign_in admin

      get :pending_enrollment_requests, params: { school_id: school.id, course_id: course.id, id: batch.id }

      expect(assigns[:pending_enrollment_requests].count).to eq(1)
    end
  end

  context "GET #approved_students" do
    it "should return approved students if request as admin or school_admin" do
      batch = create(:batch, course: course, school: school)
      student2 = create(:user, role: "student")
      create(:enrollment_request, batch_id: batch.id, student: student)
      create(:enrollment_request, batch_id: batch.id, student: student2, status: "approved", approved_by: school_admin.id, approved_at: Time.now)
      sign_in admin

      get :approved_students, params: { school_id: school.id, course_id: course.id, id: batch.id }

      expect(assigns[:approved_students].count).to eq(1)
    end

    it "should return classmates if request as student" do
      batch = create(:batch, course: course, school: school)
      student2 = create(:user, role: "student")
      create(:enrollment_request, batch_id: batch.id, student: student, status: "approved", approved_by: school_admin.id, approved_at: Time.now)
      create(:enrollment_request, batch_id: batch.id, student: student2, status: "approved", approved_by: school_admin.id, approved_at: Time.now)
      sign_in student

      get :approved_students, params: { school_id: school.id, course_id: course.id, id: batch.id }

      expect(assigns[:approved_students].count).to eq(1)
    end

    it "should return classmates if request as student and request in pending" do
      batch = create(:batch, course: course, school: school)
      student2 = create(:user, role: "student")
      create(:enrollment_request, batch_id: batch.id, student: student2, status: "approved", approved_by: school_admin.id, approved_at: Time.now)
      sign_in student

      get :approved_students, params: { school_id: school.id, course_id: course.id, id: batch.id }

      expect(assigns[:approved_students].count).to eq(0)
    end
  end

  context "GET #progress" do
    it "should completed the course if request as a student" do
      batch = create(:batch, course: course, school: school)
      enrollment_request = create(:enrollment_request, batch_id: batch.id, student: student, approved_by: school_admin.id, approved_at: Time.now)
      sign_in student

      get :progress, params: { school_id: school.id, course_id: course.id, id: batch.id }
      enrollment_request.reload

      expect(enrollment_request.progress).to eq("completed")
    end

    it "should completed the course if request as a school_admin" do
      batch = create(:batch, course: course, school: school)
      enrollment_request = create(:enrollment_request, batch_id: batch.id, student: student, approved_by: school_admin.id, approved_at: Time.now)
      sign_in school_admin

      get :progress, params: { school_id: school.id, course_id: course.id, id: batch.id }
      enrollment_request.reload

      expect(enrollment_request.progress).not_to eq("completed")
    end
  end

  context "GET #new" do
    it "should get new instance of batch whaen request as admin" do
      sign_in admin

      get :new, params: { school_id: school.id, course_id: course.id }

      expect(response.status).to eq(200)
    end

    it "should get new instance of batch whaen request as school admin" do
      sign_in school_admin

      get :new, params: { school_id: school.id, course_id: course.id }

      expect(response.body).not_to include("unauthorised")
      expect(response.status).to eq(200)
    end


    it "should get new instance of batch when request as student" do
      sign_in student

      get :new, params: { school_id: school.id, course_id: course.id }

      expect(response.body).to include("unauthorised")
      expect(response.status).to eq(302)
    end
  end
  
  context "POST #create" do
    context "validate" do
      it "admin should create batch" do
        sign_in admin

        expect do
          post :create, params: { school_id: school.id, course_id: course.id, batch: { name: "abc", start_time: Time.now, end_time: Time.now + 2.days } }
        end.to change(Batch, :count).by(1)
      end

      it "admin should create batch through api" do
        sign_in admin

        expect do
          post :create, params: {  school_id: school.id, course_id: course.id, batch: { name: "abc", start_time: Time.now, end_time: Time.now + 2.days }, format: :json }
        end.to change(Batch, :count).by(1)

        expect(response.status).to eq(201)
      end

      it "school admin should create batch through api" do
        sign_in school_admin

        expect do
          post :create, params: { school_id: school.id, course_id: course.id, batch: { name: "abc", start_time: Time.now, end_time: Time.now + 2.days }, format: :json }
        end.to change(Batch, :count).by(1)

        expect(response.status).to eq(201)
      end
    end

    context "invalid" do
      it "admin should not create course through api if params is missing" do
        sign_in admin

        expect do
          post :create, params: { school_id: school.id, course_id: course.id, batch: { start_time: Time.now, end_time: Time.now + 2.days }, format: :json }
        end.to change(Batch, :count).by(0)

        expect(response.status).to eq(422)
      end

      it "student should not create school through api" do
        sign_in student

        expect do
          post :create, params: { school_id: school.id, course_id: course.id, batch: { name: "abc", start_time: Time.now, end_time: Time.now + 2.days }, format: :json }
        end.to change(Batch, :count).by(0)

        expect(response.status).to eq(401)
      end
    end
  end

  context "PUT #update" do
    context 'with valid attributes' do
      it 'update batch attributes' do
        batch = create(:batch, course: course, school: school)
        bat = {
          name: 'abc'
        }

        sign_in admin

        put :update,  params: {
          school_id: school.id,
          course_id: course.id,
          id: batch.id,
          batch: bat
        }

        batch.reload

        expect(batch.name).to eq(bat[:name])
        expect(response.status).to eq(302)
      end

      it 'update batch attributes from API' do
        batch = create(:batch, course: course, school: school)
        bat = {
          name: 'abc'
        }

        sign_in admin

        put :update,  params: {
          school_id: school.id,
          course_id: course.id,
          id: batch.id,
          batch: bat,
          format: :json
        }

        batch.reload

        expect(batch.name).to eq(bat[:name])
        expect(response.status).to eq(200)
      end
    end

    context 'with invalid attributes' do
      it "doesn't update batch attributes" do
        batch = create(:batch, course: course, school: school)
        bat = {
          name: nil
        }

        sign_in admin

        put :update,  params: {
          school_id: school.id,
          course_id: course.id,
          id: batch.id,
          format: :json,
          batch: bat
        }

        batch.reload

        expect(batch.name).to_not eq(bat[:name])
        expect(response.status).to eq(422)
      end
    end
  end

  context 'DELETE #destroy' do
    it 'should destroy course as a admin' do
      batch = create(:batch, course: course, school: school)

      sign_in admin

      delete :destroy, params: { school_id: school.id, course_id: course.id, id: batch.id, format: :json }

      expect(response.status).to eq(204)
    end

    it 'should destroy batch as a school admin' do
      batch = create(:batch, course: course, school: school)

      sign_in school_admin

      delete :destroy, params: { school_id: school.id, course_id: course.id, id: batch.id, format: :json }

      expect(response.status).to eq(204)
    end

    it 'should not destroy batch as a student' do
      batch = create(:batch, course: course, school: school)

      sign_in student

      delete :destroy, params: { school_id: school.id, course_id: course.id, id: batch.id, format: :json }

      expect(response.status).to eq(401)
    end
  end
end