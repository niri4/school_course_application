# frozen_string_literal: true

require "rails_helper"

describe CoursesController do
  let(:student) { create(:user, role: "student") }
  let(:school_admin) { create(:user, role: "school_admin") }
  let(:admin) { create(:user, role: "admin") }
  let(:school) { create(:school, admin: school_admin) }

  context "GET #index" do
    it "should get all the courses" do
      create_list(:course, 2, school: school)
      sign_in student

      get :index, params: { school_id: school.id }

      expect(assigns[:courses].size).to eq(2)
    end
  end

  context "GET #show" do
    it "should not show course detail" do
      sign_in admin

      get :show, params: { school_id: school.id, id: "alpha" }

      expect(response.body).to include("404")
      expect(response.status).to eq(302)
    end

    it "should show course detail" do
      course = create(:course, school: school)
      sign_in admin

      get :show, params: { school_id: school.id, id: course.id }

      expect(assigns[:course].id).to eq(course.id)
    end
  end

  context "GET #new" do
    it "should get new instance of course whaen request as admin" do
      sign_in admin

      get :new, params: { school_id: school.id }

      expect(response.status).to eq(200)
    end

    it "should get new instance of course whaen request as school admin" do
      sign_in school_admin

      get :new, params: { school_id: school.id }

      expect(response.body).not_to include("unauthorised")
      expect(response.status).to eq(200)
    end


    it "should not get new instance of when request as student" do
      sign_in student

      get :new, params: { school_id: school.id }

      expect(response.body).to include("unauthorised")
      expect(response.status).to eq(302)
    end
  end
  
  context "POST #create" do
    context "validate" do
      it "admin should create course" do
        sign_in admin

        expect do
          post :create, params: { school_id: school.id, course: { name: "abc", description: "alpha" } }
        end.to change(Course, :count).by(1)
      end

      it "admin should create course through api" do
        sign_in admin

        expect do
          post :create, params: { course: { name: "abc", description: "alpha" }, school_id: school.id, format: :json }
        end.to change(Course, :count).by(1)

        expect(response.status).to eq(201)
      end

      it "school admin should create course through api" do
        sign_in school_admin

        expect do
          post :create, params: { course: { name: "abc", description: "alpha" }, school_id: school.id, format: :json }
        end.to change(Course, :count).by(1)

        expect(response.status).to eq(201)
      end
    end

    context "invalid" do
      it "admin should not create course through api if params is missing" do
        sign_in admin

        expect do
          post :create, params: { course: { description: "alpha" }, school_id: school.id, format: :json }
        end.to change(Course, :count).by(0)

        expect(response.status).to eq(422)
      end

      it "student should not create school through api" do
        sign_in student

        expect do
          post :create, params: { course: { name: "abc", description: "alpha" }, school_id: school.id, format: :json }
        end.to change(Course, :count).by(0)

        expect(response.status).to eq(401)
      end
    end
  end

  context "PUT #update" do
    context 'with valid attributes' do
      it 'update course attributes' do
        course = create(:course, school: school)
        corse = {
          name: 'abc course'
        }

        sign_in admin

        put :update,  params: {
          school_id: school.id,
          id: course.id,
          course: corse
        }

        course.reload

        expect(course.name).to eq(corse[:name])
        expect(response.status).to eq(302)
      end

      it 'update course attributes from API' do
        course = create(:course, school: school)
        corse = {
          name: 'abc course'
        }

        sign_in admin

        put :update,  params: {
          school_id: school.id,
          id: course.id,
          format: :json,
          course: corse
        }

        course.reload

        expect(course.name).to eq(corse[:name])
        expect(response.status).to eq(200)
      end
    end

    context 'with invalid attributes' do
      it "doesn't update course attributes" do
        course = create(:course, school: school)
        corse = {
          name: nil
        }

        sign_in admin

        put :update,  params: {
          school_id: school.id,
          id: course.id,
          format: :json,
          course: corse
        }

        course.reload

        expect(course.name).to_not eq(corse[:name])
        expect(response.status).to eq(422)
      end
    end
  end

  context 'DELETE #destroy' do
    it 'should destroy course as a admin' do
      course = create(:course, school: school)

      sign_in admin

      delete :destroy, params: { school_id: school.id, id: course.id, format: :json }

      expect(response.status).to eq(204)
    end

    it 'should destroy course as a school admin' do
      course = create(:course, school: school)

      sign_in school_admin

      delete :destroy, params: { school_id: school.id, id: course.id, format: :json }

      expect(response.status).to eq(204)
    end

    it 'should not destroy course as a student' do
      course = create(:course, school: school)

      sign_in student

      delete :destroy, params: { school_id: school.id, id: course.id, format: :json }

      expect(response.status).to eq(401)
    end
  end
end