# frozen_string_literal: true

require "rails_helper"

describe EnrollmentRequestsController do
  let(:student) { create(:user, role: "student") }
  let(:school_admin) { create(:user, role: "school_admin") }
  let(:admin) { create(:user, role: "admin") }
  let(:school) { create(:school, admin: school_admin) }
  let(:course) { create(:course, school: school) }
  let(:batch) { create(:batch, course: course, school: school) }

  context "GET #index" do
    it "should get all the enrollment_requests" do
      create(:enrollment_request, batch: batch, student: student)
      sign_in school_admin

      get :index, params: { batch_id: batch.id }

      expect(assigns[:enrollment_requests].size).to eq(1)
    end
  end

  context "GET #show" do
    it "should not show enrollment_requests detail" do
      sign_in admin

      get :show, params: { batch_id: batch.id, id: "alpha" }

      expect(response.body).to include("404")
      expect(response.status).to eq(302)
    end

    it "should show enrollment_requests detail" do
      student2  =  create(:user, role: "student")
      enrollment_request = create(:enrollment_request, batch_id: batch.id, student: student)
      create(:enrollment_request, batch_id: batch.id, student: student2, approved_by: school_admin.id, approved_at: Time.now)

      sign_in admin

      get :show, params: { batch_id: batch.id, id: enrollment_request.id }

      expect(assigns[:enrollment_request]).to eq(enrollment_request)
    end
  end

  context "GET #new" do
    it "should get new instance of enrollment request when request as admin" do
      sign_in admin

      get :new, params: { batch_id: batch.id }

      expect(response.status).to eq(200)
    end

    it "should get new instance of batch whaen request as school admin" do
      sign_in school_admin

      get :new, params: { batch_id: batch.id }

      expect(response.body).not_to include("unauthorised")
      expect(response.status).to eq(200)
    end


    it "should get new instance of batch when request as student" do
      sign_in student

      get :new, params: { batch_id: batch.id }

      expect(response.body).not_to include("unauthorised")
      expect(response.status).to eq(200)
    end
  end

  context "GET #approve" do
    it "should approve the enrollment request via admin" do
      enrollment_request = create(:enrollment_request, batch: batch)
      sign_in admin

      get :approve, params: { batch_id: batch.id, enrollment_request_id:  enrollment_request.id }

      enrollment_request.reload

      expect(enrollment_request.status).to eq("approved")
      expect(enrollment_request.approved_by).to eq(admin.id)
      expect(response.status).to eq(302)
    end

    it "should approve the enrollment request via school admin" do
      enrollment_request = create(:enrollment_request, batch: batch)
      sign_in school_admin

      get :approve, params: { batch_id: batch.id, enrollment_request_id:  enrollment_request.id }

      enrollment_request.reload

      expect(enrollment_request.status).to eq("approved")
      expect(enrollment_request.approved_by).to eq(school_admin.id)
      expect(response.status).to eq(302)
    end


    it "should not approve the enrollment request via student" do
      enrollment_request = create(:enrollment_request, batch: batch)
      sign_in student

      get :approve, params: { batch_id: batch.id, enrollment_request_id:  enrollment_request.id }

      enrollment_request.reload

      expect(enrollment_request.status).not_to eq("approved")
      expect(response.body).to include("unauthorised")
      expect(response.status).to eq(302)
    end
  end

  context "GET #decline" do
    it "should decline the enrollment request via admin" do
      enrollment_request = create(:enrollment_request, batch: batch)
      sign_in admin

      get :decline, params: { batch_id: batch.id, enrollment_request_id:  enrollment_request.id }

      enrollment_request.reload

      expect(enrollment_request.status).to eq("decline")
      expect(response.status).to eq(302)
    end

    it "should decline the enrollment request via school admin" do
      enrollment_request = create(:enrollment_request, batch: batch)
      sign_in school_admin

      get :decline, params: { batch_id: batch.id, enrollment_request_id:  enrollment_request.id }

      enrollment_request.reload

      expect(enrollment_request.status).to eq("decline")
      expect(response.status).to eq(302)
    end


    it "should not decline the enrollment request via student" do
      enrollment_request = create(:enrollment_request, batch: batch)
      sign_in student

      get :decline, params: { batch_id: batch.id, enrollment_request_id:  enrollment_request.id }

      enrollment_request.reload

      expect(enrollment_request.status).not_to eq("decline")
      expect(response.body).to include("unauthorised")
      expect(response.status).to eq(302)
    end
  end


  context "POST #create" do
    context "validate" do
      it "admin should create enrollment_request" do
        sign_in admin

        expect do
          post :create, params: { batch_id: batch.id, enrollment_request: { student_id: student.id } }
        end.to change(EnrollmentRequest, :count).by(1)
      end

      it "admin should create enrollment_request through api" do
        sign_in admin

        expect do
          post :create, params: { batch_id: batch.id, enrollment_request: { student_id: student.id }, format: :json }
        end.to change(EnrollmentRequest, :count).by(1)

        expect(response.status).to eq(201)
      end

      it "school admin should create enrollment_request through api" do
        sign_in school_admin

        expect do
          post :create, params: { batch_id: batch.id, enrollment_request: { student_id: student.id }, format: :json }
        end.to change(EnrollmentRequest, :count).by(1)

        expect(response.status).to eq(201)
      end

      it "student should create enrollment_request through api" do
        sign_in student

        expect do
          post :create, params: { batch_id: batch.id, enrollment_request: { student_id: student.id }, format: :json }
        end.to change(EnrollmentRequest, :count).by(1)

        expect(response.status).to eq(201)
      end
    end

    context "invalid" do
      it "admin should not create course through api if params is missing" do
        sign_in admin

        expect do
          post :create, params: { batch_id: batch.id, enrollment_request: { student_id: "hh" }, format: :json }
        end.to change(EnrollmentRequest, :count).by(0)

        expect(response.status).to eq(422)
      end
    end
  end

  context "PUT #update" do
    context 'with valid attributes' do
      it 'update enrollment_request attributes' do
        enrollment_request = create(:enrollment_request, batch: batch)
        enrollment = {
          student_id: student.id
        }

        sign_in admin

        put :update,  params: {
          batch_id: batch.id,
          id: enrollment_request.id,
          enrollment_request: enrollment
        }

        enrollment_request.reload

        expect(enrollment_request.student_id).to eq(enrollment[:student_id])
        expect(response.status).to eq(302)
      end

      it 'update enrollment_request attributes from API' do
        enrollment_request = create(:enrollment_request, batch: batch)
        enrollment = {
          student_id: student.id
        }

        sign_in admin

        put :update,  params: {
          batch_id: batch.id,
          id: enrollment_request.id,
          enrollment_request: enrollment,
          format: :json
        }

        enrollment_request.reload

        expect(enrollment_request.student_id).to eq(enrollment[:student_id])
        expect(response.status).to eq(200)
      end
    end

    context 'with invalid attributes' do
      it "doesn't update enrollment_request attributes" do
        enrollment_request = create(:enrollment_request, batch: batch)
        enrollment = {
          student_id: nil
        }

        sign_in admin

        put :update,  params: {
          batch_id: batch.id,
          id: enrollment_request.id,
          enrollment_request: enrollment,
          format: :json
        }

        enrollment_request.reload

        expect(enrollment_request.student_id).not_to eq(enrollment[:student_id])
        expect(response.status).to eq(422)
      end
    end
  end

  context 'DELETE #destroy' do
    it 'should destroy enrollment request as a admin' do
      enrollment_request = create(:enrollment_request, batch: batch, student_id: student.id)

      sign_in admin

      delete :destroy, params: { batch_id: batch.id, id: enrollment_request.id, format: :json }

      expect(response.status).to eq(204)
    end

    it 'should destroy enrollment request as a school admin' do
      enrollment_request = create(:enrollment_request, batch: batch, student_id: student.id)

      sign_in school_admin

      delete :destroy, params: { batch_id: batch.id, id: enrollment_request.id, format: :json }

      expect(response.status).to eq(204)
    end

    it 'should not destroy enrollment request as a student' do
      enrollment_request = create(:enrollment_request, batch: batch, student_id: student.id)

      sign_in student

      delete :destroy, params: { batch_id: batch.id, id: enrollment_request.id, format: :json }

      expect(response.status).to eq(401)
    end
  end
end