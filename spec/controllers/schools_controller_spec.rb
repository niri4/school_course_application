# frozen_string_literal: true

require "rails_helper"

describe SchoolsController do
  let(:student) { create(:user, role: "student") }
  let(:school_admin) { create(:user, role: "school_admin") }
  let(:admin) { create(:user, role: "admin") }

  context "GET #index" do
    it "should get all the schools" do
      create_list(:school, 2)
      sign_in student

      get :index

      expect(assigns[:schools].size).to eq(3)
    end

    it "should get all the schools where school admin is the admin" do
      create_list(:school, 2, admin: school_admin)
      sign_in school_admin

      get :index

      expect(assigns[:schools].size).to eq(2)
    end
  end

  context "GET #show" do
    it "should not show school detail" do
      sign_in admin

      get :show, params: { id: "alpha" }

      expect(response.body).to include("404")
      expect(response.status).to eq(302)
    end

    it "should not show school detail" do
      sign_in admin

      get :show, params: { id: 1 }

      expect(assigns[:school].id).to eq(1)
    end
  end

  context "GET #new" do
    it "should get new instance of school when request as admin" do
      sign_in admin

      get :new

      expect(response.status).to eq(200)
    end

    it "should get new instance of school when request as school admin" do
      sign_in school_admin

      get :new

      expect(response.body).to include("unauthorised")
      expect(response.status).to eq(302)
    end


    it "should not get new instance of school whaen request as student" do
      sign_in student

      get :new

      expect(response.body).to include("unauthorised")
      expect(response.status).to eq(302)
    end
  end
  
  context "POST #create" do
    context "validate" do
      it "admin should create school" do
        sign_in admin

        expect do
          post :create, params: { school: { name: "abc", admin_id: school_admin.id, email: "abc@example.com", address: "xyz" }}
        end.to change(School, :count).by(1)
      end

      it "admin should create school through api" do
        sign_in admin

        expect do
          post :create, params: { school: { name: "abc", admin_id: school_admin.id, email: "abc@example.com", address: "xyz"}, format: :json }
        end.to change(School, :count).by(1)

        expect(response.status).to eq(201)
      end
    end

    context "invalid" do
      it "admin should not create school through api if params is missing" do
        sign_in admin

        expect do
          post :create, params: { school: { name: "abc", email: "abc@example.com", address: "xyz"}, format: :json }
        end.to change(School, :count).by(0)

        expect(response.status).to eq(422)
      end

      it "school admin should not create school through api" do
        sign_in school_admin

        expect do
          post :create, params: { school: { name: "abc", admin_id: school_admin.id, email: "abc@example.com", address: "xyz"}, format: :json }
        end.to change(School, :count).by(0)

        expect(response.status).to eq(401)
      end

      it "student should not create school through api" do
        sign_in student

        expect do
          post :create, params: { school: { name: "abc", admin_id: school_admin.id, email: "abc@example.com", address: "xyz"}, format: :json }
        end.to change(School, :count).by(0)

        expect(response.status).to eq(401)
      end
    end
  end

  context "PUT #update" do
    context 'with valid attributes' do
      it 'update school attributes' do
        school = create(:school)
        sch = {
          name: 'abc school'
        }

        sign_in admin

        put :update,  params: {
          id: school.id,
          school: sch
        }

        school.reload

        expect(school.name).to eq(sch[:name])
        expect(response.status).to eq(302)
      end

      it 'update school attributes from API' do
        school = create(:school)
        sch = {
          name: 'abc school'
        }

        sign_in admin

        put :update,  params: {
          id: school.id,
          format: :json,
          school: sch
        }

        school.reload

        expect(school.name).to eq(sch[:name])
        expect(response.status).to eq(200)
      end
    end

    context 'with invalid attributes' do
      it "doesn't update school attributes" do
        school = create(:school)

        sch = {
          name: 'abc school',
          admin_id: nil
        }

        sign_in admin

        put :update,  params: {
          id: school.id,
          format: :json,
          school: sch
        }

        school.reload

        expect(school.name).to_not eq(sch[:name])
        expect(response.status).to eq(422)
      end
    end
  end

  context 'DELETE #destroy' do
    it 'should destroy school' do
      school = create(:school)

      sign_in admin

      delete :destroy, params: { id: school.id, format: :json }

      expect(response.status).to eq(204)
    end
  end
end