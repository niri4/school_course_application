# frozen_string_literal: true

require "rails_helper"

describe UsersController do
  let(:student) { create(:user, role: "student") }
  let(:school_admin) { create(:user, role: "school_admin") }
  let(:admin) { create(:user, role: "admin") }

  context "GET #index" do
    it "should get all the school admins" do
      create_list(:user, 2, role: "school_admin")
      sign_in admin

      get :index, params: { role: "school_admin"}

      expect(assigns[:users].size).to eq(3)
    end

    it "should get all the students" do
      create_list(:user, 2, role: "student")
      sign_in admin

      get :index, params: { role: "student"}

      expect(assigns[:users].size).to eq(4)
    end
  end

  context "GET #new" do
    it "should get new instance of user when request as admin" do
      sign_in admin

      get :new

      expect(response.status).to eq(200)
    end

    it "should not get new instance of user when request as school admin" do
      sign_in school_admin

      get :new

      expect(response.body).to include("unauthorised")
      expect(response.status).to eq(302)
    end


    it "should not get new instance of user whaen request as student" do
      sign_in student

      get :new

      expect(response.body).to include("unauthorised")
      expect(response.status).to eq(302)
    end
  end
  
  context "POST #create" do
    context "validate" do
      it "admin should create user admin" do
        sign_in admin

        expect do
          post :create, params: { user: { first_name: "john", last_name: "doe", email: "abc@example.com", role: "school_admin" }}
        end.to change(User, :count).by(1)
      end

      it "admin should create school admin through api" do
        sign_in admin

        expect do
          post :create, params: { user: { first_name: "john", last_name: "doe", email: "abc@example.com", role: "school_admin" }, format: :json }
        end.to change(User, :count).by(1)

        expect(response.status).to eq(201)
      end

      it "admin should create student through api" do
        sign_in admin

        expect do
          post :create, params: { user: { first_name: "john", last_name: "doe", email: "abc@example.com", role: "student" }, format: :json }
        end.to change(User, :count).by(1)

        expect(response.status).to eq(201)
      end
    end

    context "invalid" do
      it "admin should not create user through api if params is missing" do
        sign_in admin

        expect do
          post :create, params: { user: { first_name: "john", last_name: "doe" }, format: :json }
        end.to change(User, :count).by(0)

        expect(response.status).to eq(422)
      end

      it "school admin should not create user through api" do
        sign_in school_admin

        expect do
          post :create, params: { user: { first_name: "john", last_name: "doe", email: "abc@example.com", role: "school_admin" }, format: :json }
        end.to change(User, :count).by(0)

        expect(response.status).to eq(401)
      end

      it "student should not create user through api" do
        sign_in student

        expect do
          post :create, params: { user: { first_name: "john", last_name: "doe", email: "abc@example.com", role: "school_admin" }, format: :json }
        end.to change(User, :count).by(0)

        expect(response.status).to eq(401)
      end
    end
  end
end