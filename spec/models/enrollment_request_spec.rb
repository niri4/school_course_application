# frozen_string_literal: true

require "rails_helper"

RSpec.describe EnrollmentRequest, type: :model do
  context "Validations" do
    subject { create(:enrollment_request) }
    it { should validate_uniqueness_of(:student_id).scoped_to(:batch_id).with_message("already exists for this batch") }
  end

  context "association" do
    it { should belong_to(:batch) }
    it { should belong_to(:student) }
    it { should belong_to(:approver).class_name('User').with_foreign_key('approved_by').optional(true) }
  end

  context "approve!" do
    it 'should approve the enrollment request' do
      enrollment_request = create(:enrollment_request)
      user = create(:user, :school_admin)
      enrollment_request.approve!(user.id)
      expect(enrollment_request.approved_by).to eq(user.id)
    end 
  end

  context "progress!" do 
    it 'should update the course status to complete' do
      enrollment_request = create(:enrollment_request)
      enrollment_request.progress!
      expect(enrollment_request.progress).to eq("completed")
    end
  end
end
